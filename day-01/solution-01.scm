(use-modules
 ;; for textual reading and writing procedures
 (ice-9 textual-ports)
 ;; not sure if needed
 (ice-9 binary-ports)
 ;; for `eof-object?`
 (ice-9 rdelim)
 ;; for keyword arguments
 (ice-9 optargs)
 ;; procedures for lists
 (srfi srfi-1))

;; =================
;; HELPER PROCEDURES
;; =================

;; FILE HANDLING
(define* (get-lines-from-file file-path #:key (encoding "UTF-8"))
  ;; another common encoding is: "ISO-8859-1"
  ;; see http://www.iana.org/assignments/character-sets for more
  (define (get-lines-from-port port)
    (let ([line (get-line port)])
      (cond [(eof-object? line) '()]
            [else
             (cons line
                   (get-lines-from-port port))])))
  (call-with-input-file file-path
    (lambda (port)
      (set-port-encoding! port encoding)
      (get-lines-from-port port))))

(define (println output)
  (display (simple-format #f "~s\n" output)))

;; ============
;; ACTUAL LOGIC
;; ============
(define (main args)
  (let* ([lines (get-lines-from-file (cadr args))]
         [frequency-changes (map string->number lines)])
    (println (apply + frequency-changes))))

(main (program-arguments))
